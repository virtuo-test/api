const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const routes = require('./routes');
const app = express();
config.middlewares(app);
routes(app)

mongoose.connect(config.env.mongodb.full_path, function (err, res) {
    if(err) {
        console.log(`ERROR connecting to mongodb : ${err}`);
    }
});

app.listen(process.env.PORT || config.env.path.port, function() {
    console.log(`listening on : ${config.env.path.full_path}`);
});
module.exports = app;