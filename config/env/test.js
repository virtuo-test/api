module.exports = {
    path: {
        host: 'localhost',
        port: '4200',
        full_path: 'http://localhost:4200',
    },
    mongodb: {
        full_path: 'mongodb://localhost/virtuoTest'
    }
};
