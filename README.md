# Virtuo test

## Make it work

* git clone https://gitlab.com/virtuo-test/api.git
* npm install 
* npm run start


## Endpoints

Here is the postman documentation for all the routes available in the project : 

https://documenter.getpostman.com/view/2739395/SVmpWgw7?version=latest#1754d8d0-ff6f-4d6f-bdfb-e87ae873f7c1

To summerize the doc, there are 5 routes per "module" (cars & stations) :

*  create (name mandatory)
*  update (any field)
*  profile (to recover an object from a given id)
*  list (list of all objects filtered by the query params)
*  delete


## How it works

That's how I did it since I wasn't really sure what was expected :

*  You can create a new car without linking to a station
*  You can create a new car linking to a station (using 'station_id')
*  You can change the linking to a station with the car's update route
*  You can't add cars to a station on station's creation
*  When you delete the station the car become without station
*  A car can be without station 
*  A station can be without car

## Architecture

Here are all the parts of the program architecture : 

*  Models
*  Config (env files, constant files, middleware configs)
*  Routes (define the entrypoints of the API)
*  Actions (recover the different parameters of the request and validate/format them)
*  Controllers (communicate with the database)

## Tests

Don't forget to switch environment variable : 

"development" -> "test"

and launch **mocha --timeout 10000** in the command line to make it work.

(I've had some difficulties on my Windows environment to make everything work)


## Lib used

*  body-parser
*  express
*  mongoose
*  chai
*  mocha
*  supertest


