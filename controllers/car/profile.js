const Car = require('../../models/Car');

module.exports = function(id) {
    return Car.findOne({ _id: id })
        .populate('station');
};