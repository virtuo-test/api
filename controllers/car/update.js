const Car = require('../../models/Car');

module.exports = function(id, updatedCar) {
    return Car.updateOne({ _id: id }, updatedCar);
};