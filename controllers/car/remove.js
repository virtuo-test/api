const Car = require('../../models/Car');

module.exports = function(id) {
    return Car.deleteOne({ _id: id });
};