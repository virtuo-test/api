const Car = require('../../models/Car');

module.exports = function(params) {
    return Car.find(params)
        .populate('station');
};