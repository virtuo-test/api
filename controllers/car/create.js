const Car = require('../../models/Car');

module.exports = function(name, available, stationId) {
    const car = new Car({
        name,
        available,
        station: stationId
    });

    return car.save();
};