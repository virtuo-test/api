const Station = require('../../models/Station');
const Car = require('../../models/Car');

module.exports = function(id) {
    let station;

    return Station.findOne({ _id: id })
        .then(handleFindStationResponse)
        .then(handleFindCarsResponse);

    function handleFindStationResponse(res, err) {
        if(err) return err;
        if(!res) return Promise.reject(new Error('station not found'));
        station = res;
        return Car.find({ station: id });
    }

    function handleFindCarsResponse(res, err) {
        if(err) return err;
        station.cars = res;
        return station;
    }
};