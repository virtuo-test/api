const Station = require('../../models/Station');

module.exports = function(id) {
    return Station.deleteOne({ _id: id });
};