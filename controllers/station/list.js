const Station = require('../../models/Station');

module.exports = function(params) {
    return Station.find(params);
};