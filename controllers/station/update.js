const Station = require('../../models/Station');

module.exports = function(id, updatedStation) {
    return Station.updateOne({ _id: id }, updatedStation);
};