const Station = require('../../models/Station');

module.exports = function(name) {
    const station = new Station({
        name
    });

    return station.save();
};