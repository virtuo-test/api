const CarAction = require('../actions/car');

module.exports = function(router) {
    router.post('/cars', CarAction.create);
    router.get('/cars/:id', CarAction.profile);
    router.put('/cars/:id', CarAction.update);
    router.delete('/cars/:id', CarAction.remove);
    router.get('/cars', CarAction.list);
};