const StationAction = require('../actions/station');

module.exports = function(router) {
    router.post('/stations', StationAction.create);
    router.get('/stations/:id', StationAction.profile);
    router.put('/stations/:id', StationAction.update);
    router.delete('/stations/:id', StationAction.remove);
    router.get('/stations', StationAction.list);

};