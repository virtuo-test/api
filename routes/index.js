const config = require('../config');
const station = require('./station');
const car = require('./car');

module.exports = function(router) {
    router.get('/', function(req, res) {
        res.status(config.constants.BAD_REQUEST);
        res.json({
            message: 'Welcome to Virtuo Test',
        });
    });
    station(router);
    car(router);
};