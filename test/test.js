const app = require('../server');
const chai = require('chai');
const request = require('supertest');
const expect = chai.expect;
const mongoose = require('mongoose');

after(function (done) {
    console.log('Deleting test database');
    mongoose.connection.db.dropDatabase(done);
});

const car_one = {
    name: 'Mercedes',
    available: false
};

const car_two = {
    name: 'Ae',
    available: true
};

const station_one = {
    name: 'Montreuil'
};

let car_one_id;
let station_one_id;

describe('Virtuo Integration Tests', function() {
    describe('#GET /cars', function() {
        it('should get an empty array of cars', function(done) {
            request(app)
                .get('/cars')
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body).to.be.an('array');
                    expect(res.body).to.be.empty;
                    done();
                });
        });
    });

    describe('#POST /cars', function() {
        it('should create a car', function(done) {
            request(app)
                .post('/cars')
                .send(car_one)
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.name).to.equal('Mercedes');
                    expect(res.body.available).to.equal(false);
                    car_one_id = res.body._id;
                    done();
                });
        });

        it('should NOT create a car', function(done) {
            request(app)
                .post('/cars')
                .send(car_two)
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(400);
                    done();
                });
        });
    });

    describe('#POST /stations', function() {
        it('should create a station', function(done) {
            request(app)
                .post('/stations')
                .send(station_one)
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.name).to.equal('Montreuil');
                    station_one_id = res.body._id;
                    done();
                });
        });
    });

    describe('#PUT /cars', function() {
        it('should update a car', function(done) {
            request(app)
                .put(`/cars/${car_one_id}`)
                .send({ station_id: station_one_id })
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    done();
                });
        });
    });

    describe('#GET /cars', function() {
        it('should display the station in the car response', function(done) {
            request(app)
                .get(`/cars/${car_one_id}`)
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body.station.name).to.equal('Montreuil');
                    done();
                });
        });
    });
});
