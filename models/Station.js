const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StationSchema = Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        unique: true
    },
    cars: [{
        type: Schema.Types.ObjectId,
        ref: 'Car',
        default: null
    }]
}, { timestamps: true });

const Station = mongoose.model('Station', StationSchema);
module.exports = Station;