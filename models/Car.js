const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarSchema = Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        unique: true
    },
    available: {
        type: Boolean,
    },
    station: {
        type: Schema.Types.ObjectId,
        ref: 'Station',
        default: null
    }
}, { timestamps: true });

const Car = mongoose.model('Car', CarSchema);
module.exports = Car;