const config = require('../../config');
const StationController = require('../../controllers/station');

module.exports = function(req, res) {

    const params = {
        ...(req.query.name && { name: { $regex: `.*${req.query.name}.*` }}),
    };

    StationController.list(params)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });
};