const config = require('../../config');
const StationController = require('../../controllers/station');

module.exports = function(req, res) {
    if (!req.body.name) {
        res
            .status(config.constants.BAD_REQUEST)
            .json({
                message: "missing fields to create a new station.",
                success: false
            })
    }
    const name = req.body.name;

    StationController.create(name)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });

};