const create = require('./create');
const profile = require('./profile');
const update = require('./update');
const remove = require('./remove');
const list = require('./list');

module.exports = {
    create,
    profile,
    update,
    remove,
    list
};
