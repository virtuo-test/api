const config = require('../../config');
const StationController = require('../../controllers/station');

module.exports = function(req, res) {
    if (!req.params.id) {
        res
            .status(config.constants.BAD_REQUEST)
            .json({
                message: "station id is required.",
            })
    }

    const id = req.params.id;
    StationController.profile(id)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });
};