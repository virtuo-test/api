const config = require('../../config');
const CarController = require('../../controllers/car');

module.exports = function(req, res) {

    const params = {
        ...(req.query.name && { name: { $regex: `.*${req.query.name}.*` }}),
        ...(req.query.available && { available: req.query.available }),
    };

    CarController.list(params)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });
};