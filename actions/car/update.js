const config = require('../../config');
const CarController = require('../../controllers/car');

module.exports = function(req, res) {
    if (!req.params.id) {
        res
            .status(config.constants.BAD_REQUEST)
            .json({
                message: "car id is required.",
            })
    }

    const id = req.params.id;
    const updatedCar = {
        ...((req.body.available === false || req.body.available === true) && { available: req.body.available }),
        ...(req.body.name && { name: req.body.name }),
        ...(req.body.station_id && { station: req.body.station_id }),
    };

    CarController.update(id, updatedCar)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });
};