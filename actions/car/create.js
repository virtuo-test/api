const config = require('../../config');
const CarController = require('../../controllers/car');

module.exports = function(req, res) {
    if (!req.body.name) {
        res
            .status(config.constants.BAD_REQUEST)
            .json({
                message: "missing fields to create a new car.",
                success: false
            })
    }
    const name = req.body.name;
    const available = req.body.available || false;
    const stationId = req.body.station_id || null;

    CarController.create(name, available, stationId)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });
};