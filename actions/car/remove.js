const config = require('../../config');
const CarController = require('../../controllers/car');

module.exports = function(req, res) {
    if (!req.params.id) {
        res
            .status(config.constants.BAD_REQUEST)
            .json({
                message: "car id is required.",
            })
    }

    const id = req.params.id;

    CarController.remove(id)
        .then(result => {
            res
                .status(config.constants.OK)
                .json(result);
        })
        .catch(err => {
            res
                .status(config.constants.BAD_REQUEST)
                .json({
                    message: err.message || err
                });
        });
};